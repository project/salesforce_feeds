
README
================

OVERVIEW
-----

This module provides integration with FeedAPI (http://drupal.org/project/feedapi) and Salesforce (http://drupal.org/project/salesforce) modules. 

You can use this module to parse data from a Salesforce query and create Drupal nodes.

INSTALLATION
-----

Enable the module like usual. Copy to the sites/all/modules directory and enable
it under admin/build/modules.

CONFIGURATION
-----

This screencast provides a step by step guide to configuring and using this module to parse data from Salesforce: http://developmentseed.org/blog/2009/may/08/integrating-drupal-and-salesforce